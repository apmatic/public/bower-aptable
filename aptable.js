'use strict';

angular.module('ap.table', ['ng', 'pascalprecht.translate', 'ap.toaster', 'ap.cache', 'QuickList'])

  .factory('$tableService', function(){
    return { table: {}};
  })

  .factory('$pageinfoService', function(){
    return { pageinfo: {} };
  })

  .directive('aptable', function(
    $parse, $http, $modal, $toaster, $translate, $tableService, $cache, $q, $rootScope, $filter, $log, $location, $window) {

    var checkSortable = function(type){
      return (type === 'text') || (type === 'date') || (type === 'select') || (type === 'number');
    };

    var setNewPageSize = function($scope, callback){
      var height = window.innerHeight;
      var header = 211;
      var footer = 65;
      var theight = height - (header + footer);
      var lineheight = (parseInt($scope.tableSettings.lineheight) + 9);
      $scope.tableheight = theight;
      $scope.lineh = lineheight;
      $scope.tableSettings.pagecount = Math.floor(theight / lineheight);

      callback();
    };

    var ModalDestroyCtrl = function($scope, $modalInstance) {
      $scope.ok = function () {
        $modalInstance.close();
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    return {
      restrict: 'AE',
      scope: {
        db: '@',
        tableName: '@',
        contextSettings: '=',
        selectedSettings: '='
      },
      replace: true,
      priority: 1,
      templateUrl: 'bower_components/aptable/templates/aptable.html',
      link: function($scope, element, attrs) {
        $tableService.table[$scope.tableName] = {};
        $scope.tableSettings = $cache.tables.get($scope.tableName);
        $tableService.activeTable = $scope.tableName;

        var loadSettings = function(cb) {
          if(!$scope.tableSettings){
            $log.debug('table not in cache, request settings from server');
            // Cache does not exists -> Refresh and create cache
            $http.get('/table/settings/'+$scope.tableName).then(function (res) {
              $scope.tableSettings = res.data;

              // hard code db if necessary
              if($scope.db){
                $scope.tableSettings.db = $scope.db;
              }
              angular.copy($scope.tableSettings, $tableService.table[$scope.tableName]);
              cb();
            });
          }else{
            $log.debug('table in cache, load settings from cache');
            // Cache does exists -> just copy the data to service
            angular.copy($scope.tableSettings, $tableService.table[$scope.tableName]);
            // hard code db if necessary
            if($scope.db){
              $scope.tableSettings.db = $scope.db;
            }

            cb();
          }
        };

        loadSettings(function() {
          $scope.firstDataLoaded = false;
          $scope.loading = true;


          $scope.tableSettings = $tableService.table[$scope.tableName];
          $scope.tableSettings.selectedSettings = $scope.selectedSettings;
          $scope.activeFieldsCount = $filter('filter')($scope.tableSettings.fields, {show: true}).length;

          $scope.contclick = function(context, value, fIndex){
            if(context.click !== undefined){
              if(context.click.target === 'parent'){
                $scope.$parent[context.click.action](value, fIndex);
              }else{
                $scope[context.click.action](value, fIndex);
              }

              $('.dropdowncontext').css({ 'display' : 'none'});
              $('.tableline').removeClass('active');
            }
          };

          var selected = $scope.tableSettings.selected = [];
          $scope.selectRow = function (clickedItem, event) {
            if(!angular.element(event.target.parentElement).hasClass('cElement')){
              var foundItem = _.findWhere(selected, {_id: clickedItem._id});

              if(foundItem){ // already in array -> remove
                selected.splice(selected.indexOf(foundItem), 1); // remove item from array
              }else{ // not in array -> add
                selected.push(clickedItem);
              }
            }
          };

          $scope.checkSelected = function (id) {
            return _.findWhere(selected, {_id: id});
          };

          $scope.$on('resetSelected', function () {
            selected = $scope.tableSettings.selected = [];
          });

          $scope.showContext = function(context, value) {
            if(context.if){
              return context.if(value);
            }else{
              return true;
            }
          };

          $scope.getLink = function(context, value){
            if(context.href !== undefined){
              if(context.replace !== undefined){
                return context.href.replace('{{id}}', context.replace(value));
              }else{
                return context.href;
              }
            }
          };

          $scope.getTableClass = function(){
            if($scope.tableSettings){
              return 'line'+$scope.tableSettings.lineheight;
            }
          };

          $scope.newRequest = function(page, cb){
            $scope.loading = true;
            if(page){$scope.tableSettings.pagenr = 1;} // reset page to 1 if true
            if(!cb){cb = function() {};} // if no cb function is passed (views)

            $http.post('/search/table', $scope.tableSettings).then(function (res) {
              var data = res.data;

              $scope.data = data.data;
              $scope.tableSettings.totalitems = data.total;
              $scope.tableSettings.absolutetotal = data.absolutetotal;

              $scope.firstDataLoaded = true;
              $scope.loading = false;
              cb(data);
            });
          };

          $scope.changeSorting = function(column, type) {
            if(checkSortable(type)){
              var sort = $scope.tableSettings.sort;
              if (sort.column === column) {
                sort.descending = !sort.descending;
              } else {
                sort.column = column;
                sort.descending = false;
              }

              $scope.newRequest();
            }
          };

          $scope.selectedCls = function(column, type) {
            if(checkSortable(type)){
              if(column === $scope.tableSettings.sort.column){
                return 'sortable sort-' + $scope.tableSettings.sort.descending;
              }else{
                return 'sortable';
              }
            }
          };

          $scope.edit = function(value){
            $location.url('/app/'+$scope.tableSettings.path+'/'+value._id+'/edit');
          };

          $scope.destroy = function (value, $index) {
            var modalDestroy = $modal.open({
              templateUrl: 'bower_components/aptable/templates/destroy.html',
              controller: ModalDestroyCtrl
            });

            modalDestroy.result.then(function () {
              $http.delete('/'+$scope.tableSettings.path+'/'+value._id).then(function () {
                $toaster.pop('success', 'toaster.'+$scope.tableSettings.table, 'toaster.removed');
                $scope.newRequest(true);
              });
            }, function () {

            });
          };

          $scope.$watch('[tableSettings.lineheight, tableSettings.pagenr]', function (newNames, oldNames) {
            if($scope.tableSettings){
              setNewPageSize($scope, function(){
                $scope.newRequest();
              });
            }
          }, true);

          var saveSettings = function () {
            $cache.tables.put($scope.tableName, $tableService.table[$scope.tableName]); // write settings to localstorage cache
            $http.post('/table/settings', $tableService.table[$scope.tableName]).then(function () {
              $log.debug('tablesettings for '+$scope.tableName+' saved!');
            });
          };

          var resetTablesettings = $rootScope.$on('resetTablesettings', function(event, type) {

            $log.debug('reset tablesettings, load base settings from server');

            $http.get('/table/settings/'+$tableService.activeTable+'/defaults').then(function (res) {

              if(String(type) === 'filter'){ // reset filter
                for(var i = $scope.tableSettings.fields.length - 1; i >= 0; i--){
                  $scope.tableSettings.fields[i].search = '';
                  setNewPageSize($scope, function(){
                    $scope.newRequest();
                  });
                }
              }else{ // reset table
                $tableService.table[$scope.tableName] = {};
                $tableService.table[$scope.tableName] = angular.copy(res.data);
                saveSettings();
                $window.location.reload();
              }
            });
          });

          var settingsChanged = $rootScope.$on('settingsChanged', function () {
            saveSettings();
          });

          $scope.overview = function(item){
            $location.url('app/search/'+item._id+'/'+$scope.tableSettings.table);
          };

          // Listen for force refresh available from controllers
          $scope.$parent.$on('refreshTable', $scope.newRequest);

          $scope.$on('$destroy', function() {
            resetTablesettings(); // unbind listener for apaddomroom emit on reset settings
            settingsChanged();
            saveSettings();
          });

        });


        function removeAllContextMenus(e){
          if(!angular.element(e.target.parentElement).hasClass('cElement')){
            $('.dropdowncontext').css({ 'display' : 'none'});
            $('.tableline').removeClass('active');
          }
        }

        $(document.body).on('mousedown', removeAllContextMenus);

        $scope.$on("$destroy", function () {
          $(document.body).off('mousedown', removeAllContextMenus);
        });

      }
    };
  })

  .directive("context", function() {
    return {
      restrict: 'A',
      scope: true,
      link: function($scope, element) {

        var last = null;

        $('.dropdowncontext').css({ 'display' : 'none'});

        var showContext = function(ele, event){
          event.preventDefault();
          $('.dropdowncontext').css({ 'display' : 'none'});
          $('.tableline').removeClass('active');
          var ul = ele.children('td').first().children('ul');
          ele.addClass('active');
          ul.css({
            position: "fixed",
            display: "block",
            left: event.clientX + 'px',
            top:  event.clientY + 'px'
          });
          last = event.timeStamp;
        };

        $(element).bind('contextmenu', function(event) {
          showContext($(this), event);
        });

        $(element).bind('dblclick', function(event) {
          showContext($(this), event);
        });
      }
    };
  })

  .directive('appagination', function($parse, $translate, $tableService, $pageinfoService) {
    return {
      restrict: 'EA',
      scope: {},
      templateUrl: 'bower_components/aptable/templates/appagination.html',
      replace: true,
      link: function(scope, element, attrs) {
        var lastPage;

        scope.$evalAsync(function () {
          scope.table = $tableService.table[$tableService.activeTable];
        })

        function refresh(value) {
          if(value !== undefined) {
            updatePages();
            updatePageInfo();
          } else {
            scope.$evalAsync(function () {
              scope.table = $tableService.table[$tableService.activeTable];
            })
          }
        }

        scope.$watch('table.totalitems', function(value) {
          refresh(value);
        });

        scope.$watch('table.pagenr', function(value) {
          refresh(value);
        });

        scope.$watch('table.pagecount', function(value) {
          refresh(value);
        });

        scope.setPage = function(number){
          if((number <= lastPage) && (number >= 1)){
            scope.table.pagenr = number;
          }
        };

        function updatePageInfo(){
          var to = (scope.table.pagenr * scope.table.pagecount);
          var from = to - (scope.table.pagecount - 1);

          if(scope.table.totalitems === 0){from = 0;}

          if(to > scope.table.totalitems){to = scope.table.totalitems;}

          var filtered;
          if(scope.table.totalitems !== scope.table.absolutetotal){
            filtered = scope.table.absolutetotal;
          }

          $pageinfoService.pageinfo = {entryfrom: from, entryto: to, entrytotal: scope.table.totalitems, filtered: filtered};
        }

        function updatePages(){
          getTotalPage();

          scope.pages = [];
          scope.pages.push({text: '<<', number: 1, active: false, disabled: checkDisabledDown()});
          scope.pages.push({text: '<', number: (scope.table.pagenr - 1), active: false, disabled: checkDisabledDown()});

          var pagerange = getPageRange();

          for(var i = pagerange['start']; i <= pagerange['stop']; i++){
            scope.pages.push({text: i, number: i, active: checkActive(i), disabled: false});
          }

          scope.pages.push({text: '>', number: (scope.table.pagenr + 1), active: false, disabled: checklDisabledUp()});
          scope.pages.push({text: '>>', number: (lastPage), active: false, disabled: checklDisabledUp()});
        }

        function checkActive(i){
          return i === scope.table.pagenr;
        }

        function checkDisabledDown(){
          return scope.table.pagenr <= 1;
        }

        function checklDisabledUp(){
          return scope.table.pagenr >= lastPage;
        }

        function getPageRange(){
          var arr = [];
          if(lastPage < 5){
            arr['start'] = 1;
            arr['stop'] = lastPage;
          }else if(scope.table.pagenr < 3){
            arr['start'] = 1;
            arr['stop'] = 5;
          }else if(scope.table.pagenr > (lastPage-3)){
            arr['start'] = (lastPage - 4);
            arr['stop'] = lastPage;
          }else{
            arr['start'] = (scope.table.pagenr - 2);
            arr['stop'] = (scope.table.pagenr + 2);
          }
          return arr;
        }

        function getTotalPage(){
          var val = (parseInt(scope.table.totalitems) / parseInt(scope.table.pagecount));
          lastPage = Math.ceil(val);
        }
      }
    };
  })

  .directive('appageinfo', function($parse, $translate, $pageinfoService) {
    return {
      restrict: 'EA',
      templateUrl: 'bower_components/aptable/templates/pageinfo.html',
      replace: true,
      link: function($scope, element, attrs) {

        $scope.$watch(function(){
          return $pageinfoService.pageinfo;
        }, function(value) {
          $scope.pageinfo = value;
        });

      }
    };
  })

  .directive('apaddonroom', function($parse, $modal, $log, $tableService, $rootScope, $q, $translate, $http) {

    var ModalInstanceCtrl = function($scope, $modalInstance, addonTable, $translate) {
      $scope.addonTable = addonTable;

      $translate(['attachment']).then(function(translations) {
        $scope.translations = translations;
      });

      $scope.checkShowType = function(column) {
        if(column.type === 'file'){
          return $scope.translations['attachment'];
        }else{
          return column.text;
        }
      };

      $scope.close = function() {
        $rootScope.$emit('settingsChanged');
        $modalInstance.close();
      };
    };


    return {
      restrict: 'EA',
      scope: {},
      templateUrl: 'bower_components/aptable/templates/apaddonroom.html',
      replace: true,
      link: function(scope, element, attrs) {

        scope.$evalAsync(function () {
          scope.table = $tableService.table[$tableService.activeTable];
        })

        scope.setRow = function(row){
          $tableService.table[$tableService.activeTable].lineheight = row;
          $rootScope.$emit('settingsChanged');
        };

        scope.resetTablesettings = function(type) {
          $rootScope.$emit('resetTablesettings', type); // emit event on aptable
        };

        scope.open = function() {
          var modalInstance = $modal.open({
            templateUrl: 'bower_components/aptable/templates/modalcolumns.html',
            controller: ModalInstanceCtrl,
            resolve: {
              addonTable: function () {
                return scope.table;
              }
            }
          });
        };

        scope.selectedFunc = function (func) {
          return func(scope.table.selected);
        };

        scope.resetSelected = function () {
          $rootScope.$broadcast('resetSelected');
        };

        var checkTypeSelect = function(val, callback){

          var requests = [];

          if(val.type == "select"){

            angular.forEach(val.select, function(sel, key){
              var deferred = $q.defer();
              requests.push(deferred);

              $translate(sel.value).then(function (translation) {
                sel.bez = translation;
                deferred.resolve;
              });
            });

            $q.all(requests).then(function(){
              callback();
            });
          }else{
            callback();
          }

        };

        scope.exportTable = function(typ){
          var exptable = angular.copy(scope.table);

          var requests = [];

          angular.forEach(exptable.fields, function(val, key){
            var deferred = $q.defer();
            requests.push(deferred);

            checkTypeSelect(val, function(){

              $translate(val.text).then(function (translation) {
                val.trans = translation;
                if(typ == 'full'){
                  val.show = true;
                  val.search = '';
                  val.search1 = '';
                  val.search2 = '';
                }else if(typ == 'all'){
                  val.search = '';
                  val.search1 = '';
                  val.search2 = '';
                }

                deferred.resolve;
              });
            });
          });

          $q.all(requests).then(function(){
            if((typ == 'full') || (typ == 'all') || (typ == 'allfiltered')){
              exptable.pagecount = 9999999;
              exptable.pagenr = 1;
            }

            $http.post('/table/exports', {settings: exptable});
          });


        };


      }
    };
  });



